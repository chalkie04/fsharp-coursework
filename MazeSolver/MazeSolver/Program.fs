﻿// Learn more about F# at http://fsharp.org
// See the 'F# Tutorial' project for more help.

module MazeSolver = 
    open System

    type MazePart = 
    |Start 
    |Empty
    |Wall
    |Exit
    |Been

    let solver (li: MazePart list list) = 
        let rec start li y = 
            match li with 
            |x::xs -> 
                match List.tryFindIndex (fun x -> x = Start) x with
                |Some int  -> (int, y) 
                |None -> start xs (y+1)
            |[] -> (-1,-1)
                
        let rec exit li y = 
            match li with 
            |x::xs -> 
                match List.tryFindIndex (fun x -> x = Exit) x with
                |Some int  -> (int, y) 
                |None -> exit xs (y+1)
            |[] -> (-1,-1)    
        
        let makeRes (li:MazePart list list) = [ for i in 1..(li.Length) -> [for i in 1..((li.Item 0).Length) -> 0]] 

        let neighbours (n: (int*int)*int) (p: (int*int)*int) (li: MazePart list list)  = 
            [(1,0);(-1,0);(0,1);(0,-1)]
            |>List.map (fun (x: int*int) -> 
                try 
                    match List.item (fst (fst n) + fst x) (List.item (snd (fst n) + snd x) li) with
                    |Been -> ((fst (fst n) + fst x),(snd (fst n) + snd x)), 0
                    |Empty when ((fst (fst n) + fst x),(snd (fst n) + snd x)) <> fst p -> ((fst (fst n) + fst x),(snd (fst n) + snd x)), 1
                    |Exit -> ((fst (fst n) + fst x),(snd (fst n) + snd x)), 1
                    |_ -> (-1,-1), -1
                with 
                    |_ -> (-1,-1), -1
                )

        let rec splitlist i li (a: 'a list) =
            match li with
            |[] -> (List.rev a, [])
            |x::xs when i = 0 -> ((List.rev a), xs)
            |x::xs -> splitlist (i-1) xs (x::a)
    
        let newlist (y:int*int) (li: 'a list list) v =
            li
            |>List.mapi (fun i x ->
                match i=(snd y) with
                |true -> 
                    let lists = splitlist (fst y) x []
                    List.concat [(fst lists); [v]; (snd lists)] 
                |false -> x
            )
            
        let rec route (n :(int*int)*int) (p: (int*int)*int) li res (exit: int*int) (e: int) =
            let rec router lis  : int list list =
                match lis with 
                |x::xs when e=1 -> 
                    match x with 
                    |(-1,-1),-1 -> router xs 
                    |(a,b), c when a = fst exit && b = snd exit -> route x n li (newlist (fst n) res c) exit 0
                    |(_,_), 0 -> 
                        match List.tryFind (fun x -> snd x = 1) xs with
                        |Some ((a,b),1) when a = fst exit && b = snd exit -> route ((a,b),1) n li (newlist (fst n) res 1) exit 0
                        |Some a -> route a n (newlist (fst n) li Been) (newlist (fst n) res 1) exit e
                        |None -> route x n (newlist (fst n) li Wall) (newlist (fst n) res 0) exit e 
                    |(_,_), a -> route x n (newlist (fst n) li Been) (newlist (fst n) res 1) exit e      
                |[] -> route p n (newlist (fst n) li Been) (newlist (fst n) res 0) exit e
                |x::xs -> newlist (fst n) res 1
            router (neighbours n p li)
                    
        let mazeStart = start li 0 
        let mazeExit = exit li 0 
        let res = makeRes li     
        route (mazeStart, 1) (mazeStart, 1) li res mazeExit 1
          
    [<EntryPoint>]
    let main argv =
        let test = [[Start;Empty;Empty;Empty]
                    [Empty;Wall;Wall;Empty]
                    [Empty;Wall;Wall;Wall]
                    [Empty;Empty;Empty;Wall]
                    [Empty;Wall;Empty;Wall]
                    [Empty;Wall;Empty;Exit]
                    [Wall;Wall;Wall;Wall]] 

        let rec result (t:int list list) = 
            match t with 
            |x::xs -> printfn "%A" x ; result xs
            |[] -> printfn "Done"

        result (solver test)
        0